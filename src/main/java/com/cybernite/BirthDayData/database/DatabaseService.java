package com.cybernite.BirthDayData.database;

import java.util.Calendar;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybernite.BirthDayData.database.beans.Human;
import com.cybernite.BirthDayData.database.beans.UpdateUserBirthDayRequest;
import com.cybernite.BirthDayData.exceptions.CompanyIdAlreadyExistsException;
import com.cybernite.BirthDayData.exceptions.HumanBadRequestException;
import com.cybernite.BirthDayData.exceptions.HumanNotFoundException;
import com.cybernite.BirthDayData.exceptions.ServiceException;

@Service
public class DatabaseService {
	private final DatabaseRepository dataBaseRepository;
	private Logger logger = LoggerFactory.getLogger(DatabaseService.class); 
		
	@Autowired
	public DatabaseService(DatabaseRepository dataBaseRepository) {
		this.dataBaseRepository = dataBaseRepository;
	}
	
	/**
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public List<Human> getAllHumans() throws ServiceException {
		try {
			return this.dataBaseRepository.findAll();
		} catch (Exception e) {
			throw new ServiceException("Failed to get humans from the database.", e);
		}
	}
	
	/**
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public List<Human> getBirthdayHumans() throws ServiceException {
		try {
			Integer dayOfMonth =  Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			Integer month =  Calendar.getInstance().get(Calendar.MONTH) + 1;
			
			return this.dataBaseRepository.findByBirthDay(dayOfMonth, month);
		} catch (Exception e) {
			throw new ServiceException("Failed to get birthday humans", e);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws HumanNotFoundException 
	 */
	public Human getHumanById(String id) throws HumanNotFoundException {
		Optional<Human> human = this.dataBaseRepository.findById(id);
		if(!human.isPresent()) {
			throw new HumanNotFoundException("Failed to get human with id " + id);
		}
		
		return human.get();
	}
	
	
	/**
	 * 
	 * @param human
	 * @throws UserAlreadyExistsException 
	 * @throws HumanBadRequestException 
	 */
	public Human createUser(Human human) throws HumanBadRequestException  {
		this.dataBaseRepository.insert(human);
		logger.info("User Created succesfully with id {}.", human.getId());
		
		return human;
	}
	
	/**
	 * 
	 * @param id
	 * @throws HumanNotFoundException 
	 */
	public void deleteUser(String id) throws HumanNotFoundException {
		if(!dataBaseRepository.existsById(id)) {
			throw new HumanNotFoundException("Failed to find human with id " + id);
		}
		this.dataBaseRepository.deleteById(id);
		logger.info("User with id {} deleted successfully.", id);
	}
	
	/**
	 * 
	 * @param userDetails
	 * @return
	 * @throws HumanNotFoundException
	 */
	public Human updateUserBirthday(UpdateUserBirthDayRequest userDetails) throws HumanNotFoundException {
		
		Human human = this.getHumanById(userDetails.getId());
		human.setBirthDay(Integer.parseInt(userDetails.getBirthDay()));
		human.setBirthMonth(Integer.parseInt(userDetails.getBirthMonth()));
		this.dataBaseRepository.save(human);
		
		logger.info("User with id {} birthday updated successfully.", userDetails.getId());
		return human;
	}

	/**
	 * 
	 * @param human
	 * @return
	 * @throws CompanyIdAlreadyExistsException 
	 */
	public void validateCompanyId(Human human) throws CompanyIdAlreadyExistsException {
		if(dataBaseRepository.existsByCompanyId(human.getCompanyId())) {
			throw new CompanyIdAlreadyExistsException("Company id " + human.getCompanyId() + " already exists.");
		}
	}
}