package com.cybernite.BirthDayData.database;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybernite.BirthDayData.database.DatabaseConstants.Path;
import com.cybernite.BirthDayData.database.beans.Human;
import com.cybernite.BirthDayData.database.beans.UpdateUserBirthDayRequest;
import com.cybernite.BirthDayData.exceptions.CompanyIdAlreadyExistsException;
import com.cybernite.BirthDayData.exceptions.HumanBadRequestException;
import com.cybernite.BirthDayData.exceptions.HumanNotFoundException;
import com.cybernite.BirthDayData.exceptions.ServiceException;
import com.google.gson.Gson;

@RequestMapping(Path.MAIN)
@RestController
public class DatabaseController {
	private final DatabaseService databaseService;
	private Logger logger = LoggerFactory.getLogger(DatabaseController.class);
	
	@Autowired
	public DatabaseController(DatabaseService databaseService) {
		this.databaseService = databaseService;
	}
	
	/**
	 * Get all humans
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	@GetMapping
	public ResponseEntity<String> getAllHumans() throws ServiceException {
		try {
			List<Human> myList = databaseService.getAllHumans();
			Gson gson = new Gson();
			String json = gson.toJson(myList);
			
			logger.debug("All humans response = {}", json);
			
			return ResponseEntity.ok().body(json);
		} catch (Exception e) {
			throw new ServiceException("Failed to get all humans", e);
		}
	}
	
	/**
	 * Get birthdays
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	@GetMapping(Path.BIRTHDAY)
	public ResponseEntity<String> getBirthdayHumans() throws ServiceException {
		try {
			List<Human> myList = databaseService.getBirthdayHumans();
			Gson gson = new Gson();
			String json = gson.toJson(myList);
			
			logger.debug("Birthday humans response = {}", json);
			
			return ResponseEntity.ok().body(json);
		} catch (Exception e) {
			throw new ServiceException("Failed to get birthday humans.", e);
		}

	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws HumanNotFoundException 
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Human> getNameById(@PathVariable String id) throws HumanNotFoundException {
		
		Human human = databaseService.getHumanById(id);
		logger.debug("Request id " + id + ", returned" + human.toString());
		
		return ResponseEntity.ok().body(human);
	}
	
	/**
	 * 
	 * @param human
	 * @return
	 * @throws ServiceException 
	 * @throws HumanBadRequestException 
	 * @throws CompanyIdAlreadyExistsException 
	 */
	@PostMapping
	public ResponseEntity<Human> createUser(@RequestBody final Human human) throws ServiceException, HumanBadRequestException, CompanyIdAlreadyExistsException {
		human.validate();
		databaseService.validateCompanyId(human);
		try {
			databaseService.createUser(human);
			logger.info("Succesfully created user {}", human.toString());
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(human);
		} catch (Exception e) {
			throw new ServiceException("Failed to create user.", e);
		}
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 * @throws HumanNotFoundException 
	 */
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable String id) throws HumanNotFoundException {
		databaseService.deleteUser(id);
		return ResponseEntity.ok().build();
	}
	
	/**
	 * 
	 * @param humanDetails
	 * @return
	 * @throws HumanNotFoundException 
	 * @throws ServiceException 
	 */
	@PutMapping
	public ResponseEntity<Human> updateUserBirthday(@RequestBody UpdateUserBirthDayRequest humanDetails) throws HumanNotFoundException, ServiceException {
		try {
			Human updatedHuman = databaseService.updateUserBirthday(humanDetails);
			return ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(updatedHuman);
		} catch (HumanNotFoundException e) {
			throw e;
		} catch (Exception e) {
			throw new ServiceException("Failed to update birthday for request " + humanDetails.toString(), e);
		}
	}
}

