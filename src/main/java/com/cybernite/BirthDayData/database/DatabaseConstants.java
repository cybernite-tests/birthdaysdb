package com.cybernite.BirthDayData.database;

public class DatabaseConstants {
	public static final class User {
		public static final String BIRTH_DAY = "birthDay";
		public static final String BIRTH_MONTH = "birthMonth";
		public static final String NAME = "name";
	}
	
	public static final class Path {
		public static final String MAIN = "users";
		public static final String BIRTHDAY = "birthday";
	}
}
