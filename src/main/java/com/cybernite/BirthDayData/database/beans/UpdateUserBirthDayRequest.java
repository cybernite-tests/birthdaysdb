package com.cybernite.BirthDayData.database.beans;

public class UpdateUserBirthDayRequest {
	private String id;
	private String birthDay;
	private String birthMonth;
	
	public UpdateUserBirthDayRequest() { }
	
	public UpdateUserBirthDayRequest(String id, String birthDay, String birthMonth) {
		this.id = id;
		this.birthDay = birthDay;
		this.birthMonth = birthMonth;
	}
	
	public String getId() {
		return id;
	}
	
	public String getBirthDay() {
		return birthDay;
	}
	
	public String getBirthMonth() {
		return birthMonth;
	}
}
