package com.cybernite.BirthDayData.database.beans;

import org.springframework.data.annotation.Id;

import com.cybernite.BirthDayData.exceptions.HumanBadRequestException;

public class Human {
	@Id
	private String id;
	// Unique
	private String companyId;
	private String name;
	private Integer birthDay;
	private Integer birthMonth;
	
	public Human(String name, Integer birthDay, Integer birthMonth, String companyId) {
		this.name = name;
		this.birthDay = birthDay;
		this.birthMonth = birthMonth;
		this.companyId = companyId;
	}
	
	public Human() {}

	public String getName() {
		return name;
	}
	

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getBirthDay() {
		return birthDay;
	}

	public void setBirthDay(Integer birthDay) {
		this.birthDay = birthDay;
	}
	
	public String getId() {
		return id;
	}

	public Integer getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(Integer birthMonth) {
		this.birthMonth = birthMonth;
	}
	
	public void validate() throws HumanBadRequestException {
		
		if(companyId == null || companyId.isEmpty() ) {
			throw new HumanBadRequestException("Company id not provided.");
		}
		
		if(name == null || name.isEmpty()) {
			throw new HumanBadRequestException("Name field not provided.");
		}
		
		if(birthDay == null || birthMonth == null ) {
			throw new HumanBadRequestException("Birthday or month not provided.");
		}
		
		if(birthDay < 1 || birthDay > 31) {
			throw new HumanBadRequestException("Birthday value " + birthDay + " out of range.");
		}
		
		if(birthMonth < 1 || birthMonth > 12) {
			throw new HumanBadRequestException("Month value " + birthMonth + " out of range.");
		}
	}
	
	@Override
	public String toString() {
		return "Human [id=" + id + ", companyId=" + companyId + ", name=" + name + ", birthDay=" + birthDay
				+ ", birthMonth=" + birthMonth + "]";
	}
}
