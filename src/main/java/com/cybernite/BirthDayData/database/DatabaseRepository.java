package com.cybernite.BirthDayData.database;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.cybernite.BirthDayData.database.beans.Human;

@Repository
public interface DatabaseRepository extends MongoRepository<Human, String>{
	public List<Human> findByBirthDay(Integer BirthDay, Integer BirthMonth);
	public Boolean existsByCompanyId(String CompanyId);
}
