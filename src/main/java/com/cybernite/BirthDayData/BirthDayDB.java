package com.cybernite.BirthDayData;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BirthDayDB {

	public static void main(String[] args) {
		SpringApplication.run(BirthDayDB.class, args);
	}
}
