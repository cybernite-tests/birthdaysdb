package com.cybernite.BirthDayData.common;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.cybernite.BirthDayData.database.DatabaseController;
import com.cybernite.BirthDayData.exceptions.CompanyIdAlreadyExistsException;
import com.cybernite.BirthDayData.exceptions.HumanBadRequestException;
import com.cybernite.BirthDayData.exceptions.HumanNotFoundException;
import com.cybernite.BirthDayData.exceptions.ServiceException;

@ControllerAdvice(basePackageClasses = {DatabaseController.class})
public class GenericControllerAdvice extends ResponseEntityExceptionHandler {
	
	/**
	 * Handles service internal server errors
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = { ServiceException.class })
	protected ResponseEntity<Object> handleInternalServerError(ServiceException ex, WebRequest request) {
		String bodyOfResponse = "Internal Server Error.";
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	/**
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = { HumanNotFoundException.class })
	protected ResponseEntity<Object> handleNotFound(HumanNotFoundException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}
	
	
	/**
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = { HumanBadRequestException.class })
	protected ResponseEntity<Object> handleBadRequest(HumanBadRequestException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	/**
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(value = { CompanyIdAlreadyExistsException.class })
	protected ResponseEntity<Object> handleConflict(CompanyIdAlreadyExistsException ex, WebRequest request) {
		return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
	}
}
