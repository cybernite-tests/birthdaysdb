package com.cybernite.BirthDayData.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HumanNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(HumanNotFoundException.class);
	
	public HumanNotFoundException() {
		
	}
	
	/**
	 * 
	 * @param message
	 */
	public HumanNotFoundException(String message) {
		super(message);
		logger.info(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public HumanNotFoundException(String message, Throwable cause){
		super(message, cause);
		logger.info(message, cause);
	}
}
