package com.cybernite.BirthDayData.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HumanBadRequestException extends Exception {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(HumanBadRequestException.class);
	
	public HumanBadRequestException() {
		
	}
	
	/**
	 * 
	 * @param message
	 */
	public HumanBadRequestException(String message) {
		super(message);
		logger.info(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public HumanBadRequestException(String message, Throwable cause){
		super(message,cause);
		logger.info(message, cause);
	}
}
