package com.cybernite.BirthDayData.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceException extends Exception {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(ServiceException.class);
	
	public ServiceException() {
		
	}
	
	/**
	 * 
	 * @param message
	 */
	public ServiceException(String message) {
		super(message);
		logger.error(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public ServiceException(String message, Throwable cause){
		super(message,cause);
		logger.error(message, cause);
	}
}
