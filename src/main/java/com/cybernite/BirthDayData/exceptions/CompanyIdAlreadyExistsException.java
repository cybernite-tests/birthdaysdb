package com.cybernite.BirthDayData.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CompanyIdAlreadyExistsException extends Exception {
	private static final long serialVersionUID = 1L;
	private Logger logger = LoggerFactory.getLogger(CompanyIdAlreadyExistsException.class);
	
	public CompanyIdAlreadyExistsException() {
		
	}
	
	/**
	 * 
	 * @param message
	 */
	public CompanyIdAlreadyExistsException(String message) {
		super(message);
		logger.info(message);
	}
	
	/**
	 * @param message
	 * @param cause
	 */
	public CompanyIdAlreadyExistsException(String message, Throwable cause){
		super(message,cause);
		logger.info(message, cause);
	}
}
